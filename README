README

This chat program is half-duplex by design i.e. once a user sends a message he has to wait for a response before sending another message. The chat program can be invoked in the server mode or the client mode. Details about each mode are mentioned below.

--->>The Server Mode<<---

To invoke the server mode type the following in a terminal window :

./chat

1. In the server mode the program will bind to a local socket and wait for an incoming connection from a client (See Client mode below). 
2. Once an incoming connection is accepted and established the server will wait for an incoming message.
3. After receiving an incoming message the user at the server end will be prompted to send a message.
4. After sending the message the server will again wait for another incoming message before the user can send another message & so on.
5. The to & fro exchange of messages can continue until either the server or the client disconnects by terminating the program (Ctrl+c).

--->>The Client Mode<<---

To invoke the client mode type the following in a terminal window :

./chat -s <IP address> -p <port number>

1. In the client mode the program will try to connect to the IP address and the port number passed as argument.
2. If the server is running at the specified address a connection will be established.
3. Once the connection is established, the user at the client end will be prompted to send a message.
4. After sending the message the client will wait for an incoming message before the user can send another message & so on.
5. The to & fro exchange of messages can continue until either the server or the client disconnects by terminating the program (Ctrl+c).

Assumptions :

1. Only IPv4, no IPv6.
2. If the server has multiple IPs from multiple interfaces, the program will bind to the first IP to which it can successfully do so.

Important points from a learner's perspective :

1. When you call the getaddrinfo() function with ai_protocol=0 then you won't be assigned a port number until a successful bind has occured. After successful bind we need to call getsockname() to get the port number.
