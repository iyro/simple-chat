#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define VERSION 457

int chatClient(char *serverIP, char *serverPort);
int chatServer();

struct message 
{
	unsigned short int ver;
	unsigned short int msgLength;
	char chatMessage[140];
};
	
void print_help()
{
	fprintf(stdout, "Valid usages are : \n");
	fprintf(stdout, "  ./chat -p <port number> -s <IP address>\t:\tInitiate Chat as Client\n");
	fprintf(stdout, "  ./chat\t\t\t\t\t:\tInitiate Chat as Server\n");
	fprintf(stdout, "  ./chat -h\t\t\t\t\t:\tDisplay this Summary & exit\n\n");
	return;
}

int main ( int argc, char *argv[] )
{	
	int ret = 0, hflag = 0, pflag = 0, sflag = 0, opt = 0;
	char *serverPort, *serverIP;
	unsigned long int testip;
	
	opterr = 0;

	if (argc != 5 && argc != 1 && argc != 2)
	{
		fprintf(stdout, "Invalid Usage! Please refer to the guidelines below.\n\n");
		print_help();
		return 1;
	}
	
	while ((opt = getopt (argc, argv, "p:s:h")) != -1)
	{
		switch (opt)
		{	
			case 'h':
				hflag = 1;
				break;
			case 'p':
				pflag = 1;
				if (atoi(optarg) > 0 && atoi(optarg) <65536)
				{
					serverPort = optarg;
					break;
				}
				else
					fprintf(stdout, "Invalid port number.\n");
				return 1;
			case 's':
				sflag = 1;
				if ( inet_pton(AF_INET, optarg, &testip) != 0)
				{
					serverIP = optarg;
					break;
				}
				else
					fprintf (stdout, "Invalid IP address.\n");
				return 1;
			case '?':
				if (optopt == 'p')
					fprintf(stdout, "Error! Missing argument <port number> for option -%c. Please refer to the guidelines below.\n\n", optopt);
				else 
					if(optopt == 's')
						fprintf(stdout, "Error! Missing argument <IP address> for option -%c. Please refer to the guidelines below.\n\n", optopt);
					else
						fprintf(stdout, "Error! Unknown Option -%c. Please refer to the guidelines below.\n\n", optopt);
				print_help();
				return 1;
			default:
				perror("Unknown error!");
				abort();
		}
	}
	
	if (hflag)
	{
		if (pflag || sflag)
		{
			fprintf(stdout, "Invalid Usage! Please refer to guidelines below.");
			print_help();
			return 1;
		}
			print_help();
			return 0;
	}

	else
	{
		if (pflag && sflag)
		{	
			if ((ret = chatClient(serverIP, serverPort)) == 1)
			{
				return ret;
			}
		}
		
		if (argc == 1)
		{
			if ((ret = chatServer()) == 1)
			{
				return ret;
			}
			
		}
	}

	return 0;
}

int chatClient(char *serverIP, char *serverPort)
{
	struct addrinfo hints;
	struct addrinfo *res;
	char sendBuffer[200], ch;
	int status, chatSocket, msgSize;
	struct message sendMessage, recvMessage;
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	
	if ((status = getaddrinfo(serverIP, serverPort, &hints, &res)) != 0)
	{
		perror("getaddrinfo() error!");
		return 1;
	}
	
	if ((chatSocket = socket(PF_INET, SOCK_STREAM, res->ai_protocol)) < 0)
	{
		perror("Socket creation failed!");
		return 1;
	}
	
	fprintf (stdout, "Connecting to server... ");
	
	if (connect(chatSocket, res->ai_addr, res->ai_addrlen) < 0)
	{
		perror( "Connect failed!");
		close (chatSocket);
		return 1;
	}
	
	freeaddrinfo (res);
	fprintf (stdout, "Connected.\n");
	fprintf (stdout, "Connected to a friend! You send first.\n");
	
	while (1)
	{	
		memset(&recvMessage, 0, sizeof(recvMessage));
		memset(&sendMessage, 0, sizeof(sendMessage));
		memset(sendBuffer, '\0', sizeof(sendBuffer));
	
		while (1)
		{
			fprintf (stdout, "You: ");

			if (fgets (sendBuffer, sizeof(sendBuffer), stdin))
			{			
				msgSize = strlen(sendBuffer);
				sendBuffer[msgSize-1] = '\0';
				msgSize--;
					
				if (msgSize > 140)
				{
					fprintf (stdout, "Error: Input too long.\n");
					memset(sendBuffer, '\0', sizeof(sendBuffer));
					if ((msgSize+1) == sizeof(sendBuffer)-1)
						while((ch = getchar())!='\n'  && ch != EOF  );
					continue;
				}
			}
			break;
		}

		strcpy(sendMessage.chatMessage, sendBuffer);
		sendMessage.msgLength = htons(msgSize);
		sendMessage.ver = htons(VERSION);

		if ( send(chatSocket, &sendMessage, 4 + msgSize, 0) < 0)
		{
			perror("Send failed!");
			close (chatSocket);
			return 1;
		}

		if ( (status = recv(chatSocket, &recvMessage, sizeof(recvMessage), 0)) < 0)
		{
			perror("Receive error!");
			close (chatSocket);
			return 1;
		}

		if ( status == 0)
		{
			fprintf (stdout, "Connection closed by Server!\n");
			return 0;
		}
		if (ntohs(recvMessage.ver) == VERSION)
		{
			if (strlen(recvMessage.chatMessage) == ntohs(recvMessage.msgLength))
				fprintf (stdout, "Friend: %.140s\n", recvMessage.chatMessage);
			else
				fprintf (stdout, "Error: Malformed Packet. Message size mismatch.\n");
		}
		else
			fprintf (stdout, "Error: Malformed Packet. Version mismatch.\n");
	}
}

int chatServer()
{
	int status, sock, chatSocket, msgSize, yes = 1;
	struct addrinfo hints, *res, *p;
	struct sockaddr clientSockAddr;
	struct sockaddr_in sin;
	socklen_t clientSockAddrLen, l;
	char sendBuffer[200];
	struct message sendMessage, recvMessage;
	char host[INET_ADDRSTRLEN], ch;
	
	fprintf (stdout, "Welcome to Chat!\n");
	
	gethostname(host, sizeof(host));

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	
	if ((status = getaddrinfo(host, NULL, &hints, &res)) != 0)
	{
		perror ("getaddrinfo() error!");
		return 1;
	}
	
	for(p = res; p != NULL; p = p->ai_next)
	{	
		if ((sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0)
		{
			perror("Socket creation failed!");
			continue;
		}
		
		if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) 
		{
			perror ("setsockopt failed!");
			return 1;
		}

		if( bind(sock, p->ai_addr, p->ai_addrlen) < 0)
		{	
			perror ("Socket bind failed!");
			close (sock);
			continue;
		}		
		break;
	}
	
	if (p == NULL) 
	{
		perror("Could not bind to any socket!");
		close (sock);
		return 1;
	}
	
	freeaddrinfo (res);
	
	if (listen(sock, 2) == -1)
	{
		perror ("Listen failed!");
		close (sock);
		return 1;
	}
	
	l = sizeof(sin);
	if (getsockname(sock, (struct sockaddr *)&sin, &l) == -1)
    {
		perror ("getsockname failed!");
		return 1;
	}
	
	fprintf(stdout, "Waiting for a connection on %s port %d.\n", inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));

	clientSockAddrLen = sizeof(clientSockAddr);
	if ((chatSocket = accept(sock, &clientSockAddr, &clientSockAddrLen)) < 0)
	{
		perror ("Accept failed!");
		close (sock);
		close (chatSocket);
		return 1;
	}

	fprintf (stdout, "Found a friend! You receive first.\n");
	
	while (1)
	{	
		memset(&recvMessage, 0, sizeof(recvMessage));
		memset(&sendMessage, 0, sizeof(sendMessage));
		memset(sendBuffer, '\0', sizeof(sendBuffer));
		
		if ( (status = recv(chatSocket, &recvMessage, sizeof(recvMessage), 0)) < 0)
		{
			perror("Receive error!\n");
			close (sock);
			close (chatSocket);
			return 1;
		}
		
		if ( status == 0)
		{
			fprintf(stdout, "Connection closed by Client.\n");
			close (sock);
			close (chatSocket);
			return 0;
		}
		if (ntohs(recvMessage.ver) == VERSION)
		{
			if (strlen(recvMessage.chatMessage) == ntohs(recvMessage.msgLength))
				fprintf (stdout, "Friend: %.140s\n", recvMessage.chatMessage);
			else
				fprintf (stdout, "Error: Malformed Packet. Message size mismatch.");
		}
		else
			fprintf (stdout, "Error: Malformed Packet. Version mismatch.");
		
		while (1)
		{	
			fprintf (stdout, "You: ");
			if (fgets (sendBuffer, sizeof(sendBuffer), stdin))
			{
				msgSize = strlen(sendBuffer);
				sendBuffer[msgSize-1] = '\0';
				msgSize--;
				
				if (msgSize > 140)
				{	
					memset(sendBuffer, '\0', sizeof(sendBuffer));
					fprintf (stdout, "Error: Input too long.\n");
					if ((msgSize+1) == sizeof(sendBuffer)-1)
						while((ch = getchar())!='\n'  && ch != EOF  );
					continue;
				}
			}
			break;
		}

		strcpy(sendMessage.chatMessage, sendBuffer);
		sendMessage.msgLength = htons(msgSize);
		sendMessage.ver = htons(VERSION);
		
		if ( send(chatSocket, &sendMessage, 4 + msgSize, 0) < 0)
		{
			perror("Send failed!");
			close (sock);
			close (chatSocket);
			return 1;
		}
	}
}